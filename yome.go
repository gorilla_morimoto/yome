package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"sync"
)

func main() {
	var (
		f   *os.File
		err error
		wg  sync.WaitGroup
		arg string
	)

	wd, _ := os.Getwd()
	d, _ := os.Open(wd)
	items, _ := d.Readdir(0)
	re := regexp.MustCompile(`\.*.md`)

	if len(os.Args) == 1 {
		log.Fatal("need at least one argument")
	} else {
		arg = os.Args[1]

	}
	target, regexerr := regexp.Compile(arg)
	if regexerr != nil {
		log.Fatalln(regexerr)
	}

	for _, v := range items {
		if re.MatchString(v.Name()) {
			wg.Add(1)
			// v, fという前ををgoroutine間で使いまわすなら
			// 引数にして渡さないとv, fが無名関数にバインドされてしまって
			// 意図した動作にならない
			// https://www.kaoriya.net/blog/2013/07/08/
			go func(v os.FileInfo, f *os.File) {
				var scanner *bufio.Scanner
				f, err = os.Open(v.Name())
				if err != nil {
					log.Println(err)
					return
				}
				defer f.Close()

				var line string
				var low int

				scanner = bufio.NewScanner(f)
				for scanner.Scan() {
					low++
					line = scanner.Text()
					if target.MatchString(line) {
						fmt.Printf("[%s]:\nline %d: %s\n\n", v.Name(), low, line)
					}
				}
				wg.Done()
			}(v, f)
		}
	}
	wg.Wait()
}
